file { 'resolv.conf':
	path    => '/etc/resolv.conf',
	ensure  => file,
	source  => "/vagrant/puppet/modules/resolv/resolv.conf",
}
#exec { "/usr/bin/firewall-cmd --zone=public --add-port=2375/tcp --permanent":
#}
#exec { "/usr/bin/firewall-cmd --zone=public --add-port=2376/tcp --permanent":
#}
exec { "/usr/bin/systemctl stop firewalld":
}
exec { "/usr/bin/systemctl restart docker":
}
